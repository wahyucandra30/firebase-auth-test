import { signInWithPopup, GoogleAuthProvider } from "firebase/auth"
import { auth } from "../../firebase/firebase-config"

export const loginWithGoogle = (dispatch) => {
    const provider = new GoogleAuthProvider();
    return async () => {
        const data = await signInWithPopup(auth, provider);
        // console.log("DATA SIGN IN " + JSON.stringify(data, null, 2));
        dispatch({type: "SET_USER_DATA", payload: data});
    }
}