import { applyMiddleware } from "redux";
import { configureStore } from "@reduxjs/toolkit"
import rootReducer from "./reducers";
import thunk from "redux-thunk";
// import { composeWithDevTools } from "redux-devtools-extension";
import storage from "redux-persist/lib/storage"
import { persistReducer, persistStore } from "redux-persist"

const persistConfig = {
    key: "auth-test",
    storage
}
const persistedReducer = persistReducer(persistConfig, rootReducer);
// const store = configureStore(
//     persistedReducer,
//     composeWithDevTools(applyMiddleware(thunk))
// );

const store = configureStore({
    reducer: persistedReducer,
    middleware: [thunk],
    devTools: applyMiddleware(thunk)
});

const persistor = persistStore(store);

export { store, persistor };
