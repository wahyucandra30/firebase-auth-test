// import logo from './logo.svg';
import './App.css';
// import { useSelector } from 'react-redux';
import Login from "./pages/Login"

function App() {
  // const { userData } = useSelector((state) => state.loginReducer);
  return (
    <div className="App">
      {/* <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <h2>{userData.email}</h2>
        <h2>{userData.password}</h2>
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header> */}
      <header className="App-header">
        <Login />
      </header>
    </div>
  );
}

export default App;
