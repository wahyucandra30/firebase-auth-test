// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";
import { getAuth } from "firebase/auth"
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
    apiKey: "AIzaSyDURt2oujBDdM6_wmT7xyqmoDGt7FPV-tA",
    authDomain: "auth-test-e6553.firebaseapp.com",
    projectId: "auth-test-e6553",
    storageBucket: "auth-test-e6553.appspot.com",
    messagingSenderId: "591003161708",
    appId: "1:591003161708:web:5ea4559cf9d20bea2d7449",
    measurementId: "G-TFDWBEMQ19"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
export const analytics = getAnalytics(app);
export const auth = getAuth(app);