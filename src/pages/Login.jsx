import { useDispatch, useSelector } from "react-redux";
import { loginWithGoogle } from "../store/actions/auth";

const Login = () => {
    const dispatch = useDispatch();
    const { userData } = useSelector((state) => state.loginReducer)
    return <>
        {
            userData.user &&
            <>
                <img src={userData.user?.photoURL} alt="avatar" />
                <h1 style={{ marginBottom: "0px" }}>{userData.user.displayName}</h1>
                <span style={{ marginBottom: "24px" }}>{userData.user.email}</span>
            </>
        }
        <button onClick={loginWithGoogle(dispatch)} style={{ padding: "8px", fontWeight: "bold" }}>
            {userData.user? "Switch Account" : "Login with Google" }
        </button>

    </>
}
export default Login;